docReady(function() 
{
    r$.settings().debug = false;
    r$.settings().indent = true;
    r$.settings().useMediaQuery = true;
    r$.breakpoints(300, 767, 1280, 1600);
    //r$.breakpoints(300,480,800,1200,1600);
    var color = r$.set('background-color').values('#FF351E', '#99FF26', '#59F3FF', '#AB44FF');
    var width = r$.set('width', 'px').values(300, 300, 300, 300);
    var height = r$.set('height', 'px').values(450, 450, 450, 450);
    r$.register('printDatShit',function(w)
    {
        //console.log(w);
    });
    color.linear().applyTo('.meinBox');
    width.interval().applyTo('.meinBox');
    height.interval().applyTo('.meinBox');
    r$.start();
});

function printCssToHtml() 
{
    //console.log(document.getElementById("staticMediaQueries") ? document.getElementById("staticMediaQueries").innerHTML == '');
    var out = document.getElementById("staticMediaQueries");
    //out = String(out).replace(/\n/g, '<br/>').replace(/\t/g, '&nbsp;&nbsp;&nbsp;');
    //document.getElementById("output").innerHTML = out;
    console.log(out.sheet.cssRules[0].cssText);
}

function componentToHex(c) 
{
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function getColor(w) 
{
    var rgb = w % 255;
    return "#" + componentToHex(rgb) + componentToHex(rgb)+componentToHex(rgb);
}