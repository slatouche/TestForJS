    function getURLParameter(inParam) 
    {
        return decodeURIComponent((new RegExp('[?|&]' + inParam + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
    }

    function AjaxGET(inURL)
    {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", inURL, false);
        xhr.send();
        
        return JSON.parse(xhr.response);
    }
    
    function MakeCharList()
    {
        var charListWindow = document.getElementById("CharListWindow");
        for (i = 0; i < response["data"]["results"].length; i++)
        {
            var charDetailURL =  'charDetails.php?cID=' + response["data"]["results"][i].id;
            //console.log(charDetailURL);
            var cardDiv = document.createElement('div');
            cardDiv.id = response["data"]["results"][i].id;
            cardDiv.className = "meinBox";
            var charDetailsURL = "";
            for(ii = 0; ii < response["data"]["results"][i]["urls"].length; ii++)
            {
                if(response["data"]["results"][i]["urls"][ii].type == "detail")
                {
                    charDetailsURL = response["data"]["results"][i]["urls"][ii].url;
                }
            }
            cardDiv.onclick = function () { location.href='charDetails.php?cID='+this.id };;
            cardDiv.onmouseover = function(){this.style.opacity = "0.5"};
            cardDiv.onmouseout = function(){this.style.opacity = "1"};
            cardDiv.style ="cursor: pointer;"
            var charName = response["data"]["results"][i]["name"];
            var thumbnailPath = response["data"]["results"][i]["thumbnail"]["path"];
            var thumbnailImgType = response["data"]["results"][i]["thumbnail"]["extension"];
            var thumbnailURL = thumbnailPath + "/portrait_incredible." + thumbnailImgType;
            cardDiv.style.width = "200px";
            cardDiv.style.height = "450px";
            cardDiv.style.backgroundImage = "url('" + thumbnailURL + "')";
            cardDiv.style.backgroundSize = "100% 100%";
            cardDiv.style.margin = "5px 5px";
            cardDiv.style.display = "inline-block";
            cardDiv.innerHTML = "<span style='color:white;width:100%;margin:0 auto;float:left;background-color:rgba(255,0,0,0.5);font-size:30px;padding: 10px 0;'>" + charName + "</span>";
            charListWindow.appendChild(cardDiv);
            //charListWindow.innerHTML += "<a href='details'><div id=\"box\" class=\"meinBox\" style=\"width: 200px; height: 200px; margin: 0 auto; display:inline-block;\"></div></a>"
        }
        MakeTablePagination(response["data"].total);
    }
    
    function MakeComicList()
    {
        var charListWindow = document.getElementById("CharListWindow");
        for (i = 0; i < response["data"]["results"].length; i++)
        {
            var charDetailURL =  'charDetails.php?cID=' + response["data"]["results"][i].id;
            //console.log(charDetailURL);
            var cardDiv = document.createElement('div');
            cardDiv.id = response["data"]["results"][i].id;
            cardDiv.className = "meinBox";
            /*
            var charDetailsURL = "";
            for(ii = 0; ii < response["data"]["results"][i]["urls"].length; ii++)
            {
                if(response["data"]["results"][i]["urls"][ii].type == "detail")
                {
                    charDetailsURL = response["data"]["results"][i]["urls"][ii].url;
                }
            }
            */
            cardDiv.onclick = function () { window.location.replace(response["data"]["results"][i]["urls"][0].url.split('?')[0]) };;
            cardDiv.onmouseover = function(){this.style.opacity = "0.5"};
            cardDiv.onmouseout = function(){this.style.opacity = "1"};
            cardDiv.style ="cursor: pointer;"
            var charName = response["data"]["results"][i]["title"];
            var thumbnailPath = response["data"]["results"][i]["thumbnail"]["path"];
            var thumbnailImgType = response["data"]["results"][i]["thumbnail"]["extension"];
            var thumbnailURL = thumbnailPath + "/portrait_incredible." + thumbnailImgType;
            cardDiv.style.width = "200px";
            cardDiv.style.height = "450px";
            cardDiv.style.backgroundImage = "url('" + thumbnailURL + "')";
            cardDiv.style.backgroundSize = "100% 100%";
            cardDiv.style.margin = "5px 5px";
            cardDiv.style.display = "inline-block";
            cardDiv.innerHTML = "<span style='color:white;width:100%;margin:0 auto;float:left;background-color:rgba(0,0,255,0.7);font-size:30px;padding: 10px 0;'>" + charName + "</span>";
            charListWindow.appendChild(cardDiv);
            //charListWindow.innerHTML += "<a href='details'><div id=\"box\" class=\"meinBox\" style=\"width: 200px; height: 200px; margin: 0 auto; display:inline-block;\"></div></a>"
        }
        MakeTablePagination(response["data"].total);
    }
    
    function MakeTablePagination(inTotalNum)
    {
        var pageNum = parseInt(getURLParameter("pNum"));
        if(pageNum == null || pageNum == "" || isNaN(pageNum))
        {
            pageNum = 1;
        }
        if(pageNum > 0)
        {
            if(pageNum > 2 && pageNum < parseInt(inTotalNum/20))
            {
                var pagination = document.getElementById("myPagination");
                pagination.innerHTML += "<a href=\"?pNum=" + (pageNum-1) + "\">&laquo;</a>";
                for(i = 1; i <= ((inTotalNum/20)+1) ; i++)
                {
                    if(i >= (pageNum-2) && i <= (pageNum+2))
                    {
                        pagination.innerHTML += MakePaginationButton(i,pageNum);
                    }
                }
                pagination.innerHTML += "<a href=\"?pNum=" + (pageNum+1) + "\">&raquo;</a>";
            }
            else if(pageNum <= 2)
            {
                var pagination = document.getElementById("myPagination");
                if(pageNum > 1)
                {
                    pagination.innerHTML += "<a href=\"?pNum=" + (pageNum-1) + "\">&laquo;</a>";
                }
                for(i = 1; i <= 5 ; i++)
                {
                    pagination.innerHTML += MakePaginationButton(i,pageNum);
                }
                if(pageNum <= (inTotalNum/20))
                {
                    pagination.innerHTML += "<a href=\"?pNum=" + (pageNum+1) + "\">&raquo;</a>";
                } 
            }
            else if(pageNum >= parseInt(inTotalNum/20))
            {
                var pagination = document.getElementById("myPagination");
                if(pageNum > 1)
                {
                    pagination.innerHTML += "<a href=\"?pNum=" + (pageNum-1) + "\">&laquo;</a>";
                }
                for(i = 4; i >= 0 ; i--)
                {
                    var tempNum = Math.ceil(inTotalNum/20)-i;
                    pagination.innerHTML += MakePaginationButton(tempNum,pageNum);
                }
                if(pageNum <= (inTotalNum/20))
                {
                    pagination.innerHTML += "<a href=\"?pNum=" + (pageNum+1) + "\">&raquo;</a>";
                } 
            }
        }
        else
        {
            
        }
    }
    
    function MakePaginationButton(inCurNum, inPageNum)
    {
        var outHTML = "";
        if(inCurNum == inPageNum)
        {
            outHTML += "<a href=\"?pNum=" + inCurNum + "\" class=\"active\">" + inCurNum + "</a>";
        }
        else
        {
            outHTML += "<a href=\"?pNum=" + inCurNum + "\">" + inCurNum + "</a>";
        }
        return outHTML;
    }
    
    function Filter()
    {
        var hrefLoc = "";
        var pageNum = parseInt(getURLParameter("pNum"));
        if(pageNum == null || pageNum == "" || isNaN(pageNum))
        {
            pageNum = 0;
        }
        if(pageNum > 0)
        {
            var filterSearch = document.getElementById("filterText").value
            if(filterSearch == "" || filterSearch == null)
            {
                hrefLoc += AddToHREF(("pNum=" + pageNum), hrefLoc.length);
            }
            else
            {
                hrefLoc += AddToHREF("pNum=1", hrefLoc.length);
            }
        }
        var pageType = getURLParameter("pT");
        if(pageType == "char")
        {
            hrefLoc += AddToHREF(("pT=char"), hrefLoc.length);
        }
        else 
        {
            hrefLoc += AddToHREF(("pT=comic"), hrefLoc.length);
        }
        if(document.getElementById("charFilter").checked)
        {
            hrefLoc += AddToHREF("fT=char", hrefLoc.length);
            hrefLoc += "&fS=" + document.getElementById("filterText").value;
        }
        else if(document.getElementById("seriesFilter").checked)
        {
            hrefLoc += AddToHREF("fT=series", hrefLoc.length);
            hrefLoc += "&fS=" + document.getElementById("filterText").value;
        }
        window.location.href = hrefLoc;
    }
    
    function AddToHREF(inText, inHREFLenght)
    {
        var outText = "";
        if(inHREFLenght == 0)
        {
            outText += "?";
            outText += inText;
        }
        else if(inHREFLenght > 0)
        {
            outText += "&";
            outText += inText;
        }
        return outText;
    }
    
    var response = "";
    function GetPageData()
    {
        var pageNum = parseInt(getURLParameter("pNum"));
        if(pageNum == null || pageNum == "" || isNaN(pageNum))
        {
            pageNum = 1;
        }
        var pageType = getURLParameter("pT");
        if(pageType == "char")
        {
            document.getElementById("hideSeriesInput").hidden = true;
            var filterSearch = getURLParameter("fS");
            if(filterSearch == "" || filterSearch == null)
            {
                if(pageNum > 0)
                {
                    response = AjaxGET("http://gateway.marvel.com:80/v1/public/characters?limit=20&offset="+ ( (pageNum-1) * 20 ) +"&ts=1&apikey=b2c0be59ae77856a6784066abb0ada69&hash=529fe0c74da36c755bf311c6af4c09c6");
                }
            }
            else
            {
                response = AjaxGET("http://gateway.marvel.com:80/v1/public/characters?nameStartsWith=" + filterSearch + "&limit=20&offset="+ ( (pageNum-1) * 20 ) +"&ts=1&apikey=b2c0be59ae77856a6784066abb0ada69&hash=529fe0c74da36c755bf311c6af4c09c6");
            }
        }
        else 
        {
            var filterSearch = getURLParameter("fS");
            if(filterSearch == "" || filterSearch == null)
            {
                if(pageNum > 0)
                {
                    response = AjaxGET("http://gateway.marvel.com:80/v1/public/comics?limit=20&offset="+ ( (pageNum-1) * 20 ) +"&ts=1&apikey=b2c0be59ae77856a6784066abb0ada69&hash=529fe0c74da36c755bf311c6af4c09c6");
                }
            }
            else
            {
                var filterType = getURLParameter("fT");
                if(filterType == "char")
                {
                    response = AjaxGET("http://gateway.marvel.com:80/v1/public/comics?characters=" + filterSearch + "&limit=20&offset="+ ( (pageNum-1) * 20 ) +"&ts=1&apikey=b2c0be59ae77856a6784066abb0ada69&hash=529fe0c74da36c755bf311c6af4c09c6");
                }
                else
                {
                    response = AjaxGET("http://gateway.marvel.com:80/v1/public/comics?series=" + filterSearch + "&limit=20&offset="+ ( (pageNum-1) * 20 ) +"&ts=1&apikey=b2c0be59ae77856a6784066abb0ada69&hash=529fe0c74da36c755bf311c6af4c09c6");
                }
            }
        }
    }
    
    docReady(function() 
    {
        GetPageData();
        var pageType = getURLParameter("pT");
        if(pageType == "char")
        {
            MakeCharList();
        }
        else
        {
            MakeComicList();
        }
        console.log(response);
    });